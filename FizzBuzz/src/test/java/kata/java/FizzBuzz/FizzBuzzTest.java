package kata.java.FizzBuzz;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class FizzBuzzTest {

	public static final String FIZZ = "Fizz";
	public static final String BUZZ = "Buzz";
	public static final String FIZZ_BUZZ = "FizzBuzz";
	private FizzBuzzer fizzBuzzer;
	
	@Before
	public void before() {
		fizzBuzzer = new FizzBuzzer();
	}

	@Test
	public void shouldPrintFizzIfNumberIsDivisibleByThree() {
		assertThat(fizzBuzzer.say(6), is(equalTo(FizzBuzzTest.FIZZ)));			
	}
	@Test
	public void shouldPrintFizzFizzWhenInputIsThree() throws Exception {
		assertThat(fizzBuzzer.say(3), is(equalTo(FizzBuzzTest.FIZZ+FizzBuzzTest.FIZZ)));
	}
	
	@Test
	public void shouldPrintBuzzIFfNumberIsDivisibleByFive() throws Exception {
		assertThat(fizzBuzzer.say(5), is(equalTo(FizzBuzzTest.BUZZ)));
		assertThat(fizzBuzzer.say(10), is(equalTo(FizzBuzzTest.BUZZ)));
	}
	
	@Test
	public void shouldPrintInputNumberWhenNotMultipleOfThreeOrFive() throws Exception {
		assertThat(fizzBuzzer.say(7), is(equalTo("7")));
		assertThat(fizzBuzzer.say(17), is(equalTo("17")));
	}
	
	@Test
	public void shouldPrintFizzBuzzWhenNumberIsMultipleOfThreeAndFive() throws Exception {
		assertThat(fizzBuzzer.say(15), is(equalTo(FizzBuzzTest.FIZZ_BUZZ)) );
		assertThat(fizzBuzzer.say(45), is(equalTo(FizzBuzzTest.FIZZ_BUZZ)) );
	}
	@Test
	public void shouldPrintFizzWhenNumberContainsThree() throws Exception {
		assertThat(fizzBuzzer.say(137), is(equalTo(FizzBuzzTest.FIZZ)));
		assertThat(fizzBuzzer.say(7331), is(equalTo(FizzBuzzTest.FIZZ)));
	}
	@Ignore
	public void fizzShouldSupportNullInput() throws Exception {
		Fizz fizz2 = new Fizz();
		StringBuffer sb = null;
		int i = 0;
		assertThat(fizz2.say(i,sb).toString(), is(equalTo(FizzBuzzTest.FIZZ)));
	}
	
	
}
