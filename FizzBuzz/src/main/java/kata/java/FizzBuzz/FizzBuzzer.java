package kata.java.FizzBuzz;


public class FizzBuzzer {

	private Fizz entryPoint;
	
	public FizzBuzzer() {
		entryPoint = new Fizz();
		
		Buzz buzz = new Buzz();
		entryPoint.setNext(buzz);
		
		FizzCointainsThree fizzCointainsThree = new FizzCointainsThree();
		buzz.setNext(fizzCointainsThree);
		
		fizzCointainsThree.setNext(new Identity());
	}
	
	public String say(int i) {
		StringBuffer buffer = entryPoint.say(i, new StringBuffer());
		return buffer.toString();
	}


}
