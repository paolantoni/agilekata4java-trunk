package kata.java.FizzBuzz;

public class NumberUtils {

	public static boolean isMultipleOf(int number, int factor) {
		return number%factor==0;
	}
	
	private NumberUtils() {};

}
