package kata.java.FizzBuzz;

public interface FizzBuzz {
	StringBuffer say(int i, StringBuffer sb);
	void setNext(FizzBuzz st);
}
