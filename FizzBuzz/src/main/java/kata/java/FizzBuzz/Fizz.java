package kata.java.FizzBuzz;

class Fizz implements FizzBuzz {

	private FizzBuzz fizzBuzz;
	
	public StringBuffer say(int i, StringBuffer sb) {
		
		if ( NumberUtils.isMultipleOf(i, 3)){
			sb.append("Fizz");
		}
		
		 return fizzBuzz.say(i,sb);
	}

	
	@Override
	public void setNext(FizzBuzz st) {
		fizzBuzz = st;
	}

}
