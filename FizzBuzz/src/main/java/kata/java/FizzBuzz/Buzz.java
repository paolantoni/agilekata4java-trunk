package kata.java.FizzBuzz;

class Buzz implements FizzBuzz {

	private FizzBuzz fizzBuzz;
	
	public StringBuffer say(int i, StringBuffer sb) {
			
		if (NumberUtils.isMultipleOf(i, 5)){
			sb.append("Buzz");
		}
		return  fizzBuzz.say(i, sb);
	}

	@Override
	public void setNext(FizzBuzz st) {
		fizzBuzz = st;
		// TODO Auto-generated method stub
		
	}

}
