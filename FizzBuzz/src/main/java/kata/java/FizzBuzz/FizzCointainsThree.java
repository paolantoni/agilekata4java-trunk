package kata.java.FizzBuzz;

class FizzCointainsThree implements FizzBuzz {

	private FizzBuzz fizzBuzz;
	
	@Override
	public StringBuffer say(int i, StringBuffer sb) {
		if(String.valueOf(i).contains("3")) {
			sb.append("Fizz");
		}
		return fizzBuzz.say(i, sb);
	}

	@Override
	public void setNext(FizzBuzz st) {
		fizzBuzz = st;
	}

}
