package kata.java.FizzBuzz;

public class Identity implements FizzBuzz {

	@Override
	public StringBuffer say(int i, StringBuffer sb) {
		if (sb.length() == 0){
			 sb.append(i);
		}
		return sb;
	}

	@Override
	public void setNext(FizzBuzz st) {
		//should provide a null implementation.
	}

}
