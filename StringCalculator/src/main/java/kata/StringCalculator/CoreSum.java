package kata.StringCalculator;

abstract class CoreSum implements Sum {

	protected String[] splittedTerms;

	public int calculate() {
		return calculate(splittedTerms);
	}

	private int calculate(String[] splittedTerms) {
		int sum=0;
		for (String addendum : splittedTerms) {
			sum += Integer.parseInt(addendum);					
		}
		return sum;
	}

}
