package kata.StringCalculator;

class SumFactory {

	public static Sum createBy(String delimiter, String addendums) {
		if(isNotEmpty(addendums)) {
			if(addendums.startsWith("//")) {
				return new SumWithCustomDelimiter(delimiter, addendums);
			} else {
				return new SumWithStandardDelimiter(delimiter, addendums);
			}
		}
		return Sum.NULL;
	}
	
	private static boolean isNotEmpty(String addendums) {
		return ! "".equals(addendums);
	}

}
