package kata.StringCalculator;

public interface Sum {
	int calculate();
	
	public Sum NULL = new Sum() {
		
		@Override
		public int calculate() {
			return 0;
		}
	};  
}
