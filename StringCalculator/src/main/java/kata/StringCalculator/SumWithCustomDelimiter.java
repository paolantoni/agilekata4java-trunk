package kata.StringCalculator;

class SumWithCustomDelimiter extends CoreSum {

	public SumWithCustomDelimiter(String delimiter, String addendums) {
		delimiter = customDelimiter(addendums);
		addendums = addendums.substring(4);
		splittedTerms = addendums.split(delimiter);
	}

	private String customDelimiter(String addendums) {
		return addendums.charAt(2)+"";
	}
}
