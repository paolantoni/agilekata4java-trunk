package kata.StringCalculator;

public class StringCalculator {

	private static final String STANDARD_DELIMITER_EXPRESSION = ",|\n";

	public int add(String addendums) {
		return SumFactory.createBy(STANDARD_DELIMITER_EXPRESSION, addendums).calculate();
	}
}
