package kata.StringCalculator;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class StringCalculatorTest_PlainJunit {

	private StringCalculator calculator;

	@Before
	public void before(){
		calculator = new StringCalculator();
	}
	
	@Test
	public void addMethod_shouldReturnZeroWhenEmptyInput() {
		assertEquals(0, calculator.add(""));
		
	}
	
	@Test
	public void addMethod_shouldReturnSameInputWhenOneNumberInput() {
		assertEquals(5, calculator.add("5"));
		
	}
	
	@Test
	public void shouldReturnTheSumWhenMultipleInputSeparatedByCommas(){
		assertEquals(5, calculator.add("2,3"));
		assertEquals(18, calculator.add("9,9"));
	}
	

}
