package kata.StringCalculator;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

public class StringCalculatorTest_Hamcrest {

	private StringCalculator calculator;

	@Before
	public void before(){
		calculator = new StringCalculator();
	}
	
	@Test
	public void addMethod_shouldReturnZeroWhenEmptyInput() {
		assertThat(calculator.add(""), is(equalTo(0)));
		
	}
	
	@Test
	public void addMethod_shouldReturnSameInputWhenOneNumberInput() {
		assertThat(calculator.add("5"), is(equalTo(5)));
		
	}
	
	@Test
	public void shouldReturnTheSumWhenMultipleInputSeparatedByCommas(){
		assertThat(calculator.add("2,3"), is(equalTo(5)));
		assertThat(calculator.add("9,9"), is(equalTo(18)));
	}
	
	@Test
	public void shouldaddIndefiniteArgumentNumbers() throws Exception {
		assertThat(calculator.add("2,6,4,5,30"), is(equalTo(47)));
	}

	@Test
	public void shouldAcceptAlsoNewLineAsDelimiter() throws Exception {
		assertThat(calculator.add("1\n2,3"), is(equalTo(6)));
	}
	
	@Test
	public void shouldAcceptAConfigurableDelimiter() throws Exception {
		assertThat(calculator.add("//;\n2;3"), is(equalTo(5)));
	}
	
}
