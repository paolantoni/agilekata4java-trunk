package org.arquillian.example;

import static org.junit.Assert.fail;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
	

/**
 * Un test Arquillian deve avere necessariamente tre cose:
 * 
 * 1)Un’annotazione @RunWith(Arquillian.class) sulla classe 
 * 2)Un metodo statico pubblico annotato con @Deployment che ritorna un archivio di ShrinkWrap 
 * 3)Almeno un metodo annotato con @Test 
 * 
 * L’annotazione @RunWith dice a JUnit di usare Arquillian come test controller. 
 * Arquillian cerca in seguito un metodo statico pubblico annotato con 
 * l’annotazione @Deployment per ricevere l’archivio di test (p.es., micro-deployment). 
 * Così automaticamente ogni metodo @Test girerà dietro un container.
 * @author federico
 *
 *
 * Non ci sarà bisogno dei tipici metodi JUnit di setup e tearDown 
 * dal momento che tutto sarà gestito da Arquillian. 
 */

@RunWith(Arquillian.class)
public class GreeterTest {
	
	
	/**
	 * Cos’è un archivio di test?
	 * Lo scopo dell’archivio di test è di isolare le classi 
	 * e le risorse necessarie al test dal resto del classpath. 
	 * Diversamente da un normale test unitario, il test di Arquillian non 
	 * deve importare tutto il classpath del progetto. 
	 * Al contrario, verrà incluso soltanto quello che il test necessita 
	 * (che può essere anche l’intero classpath, se lo vogliamo). 
	 * L’archivio è definito usando ShrinkWrap, 
	 * che è una serie di API Java per creare archivi Java (p.es., jar, war, ear). 
	 * La strategia del micro-deployment permette di focalizzarsi 
	 * con precisione sulle classi che vogliamo testare. 
	 * Come risultato, il test sarà molto leggero e maneggevole.
	 * L’archivio ShrinkWrap installato sul server, 
	 * è archivio reale a tutti gli effetti. 
	 * Il deployer del container non sa che sta per deployare un archivio ShrinkWrap. 
	 * Si può pensare a ShrinkWrap come un tool di build basato su Java, 
	 * con una differenza: nel caso ottimale, passa l’archivio in stream sul
	 *  server anzichè scriverlo su disco. Ma l’archivio è comunque autentico.
	 * @return jar
	 */
	@Deployment
	public static JavaArchive createDeployment() {
//		Con ShrinkWrap, abbiamo definito un archivio Java (jar) come deployment. 
//		L’archvio include la classe Greeter che il test invocherà e un beans.xml vuoto 
//		nella directory META-INF per attivare il CDI in quest’archivio.
		JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
		        .addClass(Greeter.class)
		        .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		
//		stampo i contenuti dell’archivio che ShrinkWrap crea durante l’esecuzione del test	
		    System.out.println(jar.toString(true));
		
		    return jar;
	}

	@Test
	public void shouldCreateGreetings() {
		fail("Not yet implemented");
	}

}
