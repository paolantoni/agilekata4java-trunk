package org.arquillian.example;

import java.io.PrintStream;

public class Greeter {

	/**
	 * A component for creating personal greetings.
	 * Lo scopo è di verificare che questa classe si comporti
	 *  nel modo giusto quando viene invocata tramite un bean CDI. 
	 *  Naturalmente, lo facciamo scrivendo un semplice test unitario. 
	 *  Abbiamo però bisogno che il bean utilizzi i servizi enterprise 
	 *  come la dependency injection e il messaging e che venga usato 
	 *  da dentro un container. (Inoltre gli daremo spazio per crescere )
	 *  Per usare la classe come bean CDI, 
	 *  dobbiamo importarlo dal test con l’annotazione@@Inject@. 
	 *  E verrà fatto dal test di Arquillian! 
	 */

	public void greet(PrintStream to, String name) {
		to.println(createGreeting(name));
	}

	public String createGreeting(String name) {
		return "Hello, " + name + "!";
	}

}
