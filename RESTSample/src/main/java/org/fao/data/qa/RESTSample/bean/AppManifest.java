package org.fao.data.qa.RESTSample.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class AppManifest {


	private @Context ServletContext context;
	private String implementationVersion;
	private String svnRevision = "";
	private String buildTime= "";
	private String svnTag= "";
	private Manifest manifest;	

	public AppManifest(){}
	
	public AppManifest buildFrom(InputStream inputStream){
		try {
			manifest = new Manifest(inputStream);
			System.out.println(manifest.getEntries());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Attributes attributes=manifest.getMainAttributes();
		this.implementationVersion = attributes.getValue("Implementation-Version");
		this.buildTime = attributes.getValue("Build-Time");
		this.svnRevision = attributes.getValue("HudsonSvnRevision");
		this.svnTag = attributes.getValue("HudsonSvnTag");
		return this;
	}
	

	@XmlElement
	public String getImplementationVersion(){
		return implementationVersion;
	}
	@XmlElement
	public String getSvnRevision() {
		return svnRevision;
	}

	@XmlElement
	public String getBuildTime() {
		return buildTime;
	}

	@XmlElement
	public String getSvnTag() {
		return svnTag;
	}
}
