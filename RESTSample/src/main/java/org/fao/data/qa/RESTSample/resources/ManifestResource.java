package org.fao.data.qa.RESTSample.resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.fao.data.qa.RESTSample.bean.AppManifest;

@Path("/manifest")
public class ManifestResource {
	
	private @Context ServletContext context;
	
	public ManifestResource() {
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public AppManifest getManifestAsXML() throws IOException{
		
		InputStream inputStream = context.getResourceAsStream("/META-INF/MANIFEST.MF");
		
	
		AppManifest appManifest = new AppManifest().buildFrom(inputStream);
		return appManifest;
	}

}
