package org.fao.data.qa.RESTSample.utils;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.sun.grizzly.http.SelectorThread;
import com.sun.grizzly.http.servlet.ServletAdapter;
import com.sun.jersey.api.container.grizzly.GrizzlyServerFactory;
import com.sun.jersey.spi.container.servlet.ServletContainer;

public class GrizzlyMain {
 
	public final static URI baseUri = UriBuilder.fromUri( "http://localhost/" ).port( 9998 ).build();
 
    public static SelectorThread startServer() throws IOException {
        
        final ServletAdapter adapter =new ServletAdapter();
    	adapter.addInitParameter( "com.sun.jersey.config.property.packages", "org.fao.data.qa.RESTSample.resources" );
    	adapter.addInitParameter( "com.sun.jersey.api.json.POJOMappingFeature", "true" );

    	adapter.setContextPath(baseUri.getPath());
    	adapter.setProperty( "load-on-startup", 1 );
    	adapter.setServletPath("/RESTSample/rest");
//    	adapter.setResourcesContextPath("rest");
    	adapter.setServletInstance(new ServletContainer());
    	System.out.println(adapter.getResourcesContextPath());

    	System.out.println("********" + baseUri.getPath());
        SelectorThread threadSelector = GrizzlyServerFactory.create(baseUri, adapter);
        return threadSelector;
    }
     
    public static void main(String[] args) throws IOException {
        System.out.println("Starting grizzly...");
        SelectorThread threadSelector = startServer();
        System.out.println(String.format(
                "Jersey app started with WADL available at %sRESTSample/rest/application.wadl\n" +
                "Hit enter to stop it...", baseUri));
        System.in.read();
        threadSelector.stopEndpoint();
    }    
}
