package org.fao.data.qa.RESTSample.resources;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.io.IOException;
import java.util.Arrays;

import javax.ws.rs.core.MediaType;

import org.fao.data.qa.RESTSample.bean.Address;
import org.fao.data.qa.RESTSample.bean.Contact;
import org.fao.data.qa.RESTSample.utils.GrizzlyMain;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class ContactsTest  {


	private static Client c;
	private static WebResource r;
	private static ClientResponse response;
	static SelectorThread st;
	private static ClientConfig clientConfiguration;

	//http://ameethpaatil.blogspot.it/2011/09/restful-webservice-jersey-spring-30x.html

	@BeforeClass
	public static void startServer() throws IOException{
		st = GrizzlyMain.startServer(); //HERE I COULD START A TOMCAT SERVER OR JETTY OR JBOSS JUST PROVIDE THE CORRECT IMPLEMENTATION
		clientConfiguration = new DefaultClientConfig();
		c = Client.create(clientConfiguration);
		r = c.resource("http://localhost:9998/RESTSample/rest/contacts");
		response = r.get(ClientResponse.class);
	}


	/**
	 * The core class for the client is the WebResource class. 
	 * You use it to build a request URL based on the root URI, 
	 * and then send requests and get responses. 
	 * Listing 10 shows how to create a WebResource instance. 
	 * Notice that WebResource is a heavy object, so you create it once.
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		//		c = Client.create();
		//		r = c.resource("http://localhost:8080/RESTSample/rest/contacts");
		//		response = r.get(ClientResponse.class);
		//		 SelectorThread st = GrizzlyMain.startServer();
		
	}

	@Test
	public void testContactsServiceStatusIs200() {
		Assert.assertThat(response.getStatus(), is(200));
	}

	@Test
	public void testConcatsServiceContentTypeIsApplicationXML(){
		Assert.assertThat(""+response.getHeaders().get("Content-Type"), is(equalTo("[application/xml]")));
	}

	@Test
	public void testContactsEntitiesExist(){
		String entity = response.getEntity(String.class);		
		//		System.out.println(entity);
		Assert.assertTrue(entity.contains("Huang Yi Ming"));
	}

	@Test
	public void testCreateContact(){
		Address[] addrs = {
				new Address("Shanghai", "Ke Yuan Street")
		};
		Contact c = new Contact(""+Math.random(), "Foo Bar", Arrays.asList(addrs));

		ClientResponse response = r
				.path(c.getId())
				.accept(MediaType.APPLICATION_XML)
				.put(ClientResponse.class, c);
		int status = response.getStatus();			
		//		System.out.println(status);
		Assert.assertThat(status, is(201));
	}

	
	@AfterClass
	public static void stop(){
		c.destroy();
		st.stopEndpoint();
	}
}
