package kata.java.tennis;

class Score {

	public int firstPlayer;
	public int secondPlayer;

	public Score(int _1stPlayerScore, int _2ndPlayerScore) {
		this.firstPlayer = _1stPlayerScore;
		this.secondPlayer = _2ndPlayerScore;
	}

}
