package kata.java.tennis;

public class SecondPlayer implements Player {

	private String name;

	public SecondPlayer(String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

}
