package kata.java.tennis;

public class FirstPlayer implements Player {

	private String name;

	public FirstPlayer(String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return this.name;
	}

}
