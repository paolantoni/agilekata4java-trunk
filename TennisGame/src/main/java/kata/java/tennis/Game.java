package kata.java.tennis;

public class Game {

	private static final int DOUBLE_ADVANTAGE = 60;
	private static final int ADVANTAGE = 50;
	private String _1stPlayer;
	private String _2ndPlayer;
	private Score gameScoringPoints;
	private FirstPlayer firstPlayer;
	private SecondPlayer secondPlayer;

	public Game(String _1stPlayer, String _2ndPlayer) {
		if( _1stPlayer == _2ndPlayer) {
			throw new IllegalArgumentException();
		}
		this._1stPlayer = _1stPlayer;
		this._2ndPlayer = _2ndPlayer;
		this.gameScoringPoints = new Score(0,0);
	}
	
	public Game(FirstPlayer firstPlayer, SecondPlayer secondPlayer) {
		this(firstPlayer.name(), secondPlayer.name());
	}

	public String score() {
		return printScore(gameScoringPoints.firstPlayer)+"-"+printScore(gameScoringPoints.secondPlayer);
	}

	private String printScore(int actualScore) {
		if(actualScore == ADVANTAGE) {
			return "AD";
		}
		if(actualScore == DOUBLE_ADVANTAGE) {
			return "ADAD";
		}
		return actualScore+"";
	}

	public void firstPlayerScore() {
		gameScoringPoints = increaseFirstPlayerScore(gameScoringPoints.firstPlayer, gameScoringPoints.secondPlayer);
	}
	
	public void secondPlayerScore() {
		gameScoringPoints = increaseSecondPlayerScore(gameScoringPoints.secondPlayer, gameScoringPoints.firstPlayer);
	}
	
	private Score increaseFirstPlayerScore(int actualWinnerScore, int actualLooserScore) {
		int newWinnerScore = newWinnerScore(actualWinnerScore, actualLooserScore);
		int newLooserScore = newLooserScore(actualWinnerScore, actualLooserScore);
		return new Score(newWinnerScore, newLooserScore);
	}
	
	private Score increaseSecondPlayerScore(int actualWinnerScore, int actualLooserScore) {
		int newWinnerScore = newWinnerScore(actualWinnerScore, actualLooserScore);
		int newLooserScore = newLooserScore(actualWinnerScore, actualLooserScore);
		return new Score(newLooserScore, newWinnerScore);
	}

	private int newLooserScore(int scoringPlayerScore, int otherPlayerScore) {
		if(scoringPlayerScore == 40) {
			if (otherPlayerScore == ADVANTAGE) { 
				otherPlayerScore = 40;
			}
		} 
		return otherPlayerScore;
	}

	private int newWinnerScore(int scoringPlayerScore, int otherPlayerScore) {
		int newScore = 0;
		if(scoringPlayerScore == 0) {
			newScore = 15;
		} else
		if(scoringPlayerScore == 15) {
			newScore = 30;
		} else
		if(scoringPlayerScore == 30) {
			newScore = 40;
		} else
		if(scoringPlayerScore == 40) {
			if (otherPlayerScore == ADVANTAGE) { 
				newScore = 40;
			} else {
				newScore = ADVANTAGE;
			}	
		} else
		if(scoringPlayerScore == ADVANTAGE) {
			newScore = DOUBLE_ADVANTAGE;
		}
		return newScore;
	}

	
	
	


	public String winner() {
		if(gameScoringPoints.firstPlayer > gameScoringPoints.secondPlayer) {
			return _1stPlayer;
		}
		return _2ndPlayer;
	}

}
