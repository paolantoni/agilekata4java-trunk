package kata.java.tennis;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

	private static final String _1stPlayer = "Federer";
	private static final String _2ndPlayer = "Murray";
	private Game game;
	
	@Before
	public void before() {
		game = new Game("first", "second");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void playersShouldBeDifferent() throws Exception {
		game = new Game((String)null, null);
	}

	@Test
	public void 
	startingScoreIsZeroZero() {
		assertThat(game.score(), is("0-0"));
	}
	
	@Test
	public void _1stPlayerScoreShouldFollow_15_30_40_sequence() throws Exception {
		game.firstPlayerScore();
		assertThat(game.score(), is("15-0"));
		
		game.firstPlayerScore();
		assertThat(game.score(), is("30-0"));
		
		game.firstPlayerScore();
		assertThat(game.score(), is("40-0"));
	}
	
	@Test
	public void _2ndPlayerScoreShouldFollow_15_30_40_sequence() throws Exception {
		game.secondPlayerScore();
		assertThat(game.score(), is("0-15"));
		
		game.secondPlayerScore();
		assertThat(game.score(), is("0-30"));
		
		game.secondPlayerScore();
		assertThat(game.score(), is("0-40"));
	}
	
	@Test
	public void theScorerShouldGoToAdvantageIfBothAt_40_() throws Exception {
		gameStartsFromDeuce();
		
		game.firstPlayerScore();
		assertThat(game.score(), is("AD-40"));
		
		gameStartsFromDeuce();
		game.secondPlayerScore();
		assertThat(game.score(), is("40-AD"));
		
	}

	@Test
	public void playerWinsWhenDoubleAdvantage() throws Exception {
		gameStartsFromDeuce();
		game.firstPlayerScore();
		game.firstPlayerScore();
		
		assertThat(game.score(), is("ADAD-40"));
		assertThat(game.winner(), is(_1stPlayer));
		
		gameStartsFromDeuce();
		game.secondPlayerScore();
		game.secondPlayerScore();
		
		assertThat(game.score(), is("40-ADAD"));
		assertThat(game.winner(), is(_2ndPlayer));
	}

	private void gameStartsFromDeuce() {
		game = new Game(_1stPlayer, _2ndPlayer);
		game.firstPlayerScore();
		game.firstPlayerScore();
		game.firstPlayerScore();
		
		game.secondPlayerScore();
		game.secondPlayerScore();
		game.secondPlayerScore();
		
		assertThat(game.score(), is("40-40"));
	}
	
	@Test
	public void shouldGoBackToDeuce() throws Exception {
		gameStartsFromDeuce();
		game.firstPlayerScore();
		assertThat(game.score(), is("AD-40"));
		
		game.secondPlayerScore();
		assertThat(game.score(), is("40-40"));
	}
}

